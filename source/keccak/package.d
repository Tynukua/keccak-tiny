/// Port of C keccak-tiny library 
/// Implements keccakf-1600 functions
/// See_Also: https://github.com/IoTone/keccak-tiny
/// Authors: Tynuk

module keccak;

private static immutable ubyte[24] rho = [
    1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 2, 14, 27, 41, 56, 8, 25, 43, 62, 18, 39,
    61, 20, 44
];
private static immutable ubyte[24] pi = [
    10, 7, 11, 17, 18, 3, 5, 16, 8, 21, 24, 4, 15, 23, 19, 13, 12, 2, 20, 14, 22,
    9, 6, 1
];
private static immutable ulong[24] RC = [
    1, 0x8082, 0x800000000000808a, 0x8000000080008000, 0x808b, 0x80000001,
    0x8000000080008081, 0x8000000000008009, 0x8a, 0x88, 0x80008009,
    0x8000000a, 0x8000808b, 0x800000000000008b, 0x8000000000008089,
    0x8000000000008003, 0x8000000000008002, 0x8000000000000080, 0x800a,
    0x800000008000000a, 0x8000000080008081, 0x8000000000008080, 0x80000001,
    0x8000000080008008
];

private enum PLEN = 200;

private auto rol(T)(T x, T s)
{
    return ((x) << s) | ((x) >> (64 - s));
}

pragma(inline, true) private struct State
{
    import std.bitmanip;

    ubyte[PLEN] a;

    void opIndexAssign(ulong v, size_t i) pure nothrow @safe @nogc
    {
        i *= ulong.sizeof;
        a[i .. i + ulong.sizeof] = v.nativeToLittleEndian;
    }

    ulong opIndex(size_t i) pure nothrow @safe @nogc
    {
        i *= ulong.sizeof;
        ubyte[ulong.sizeof] t;
        t[] = a[i .. i + ulong.sizeof];
        return t.littleEndianToNative!ulong;

    }

    void opIndexOpAssign(string op)(ulong v, size_t i) pure nothrow @safe @nogc
    {
        mixin(q{this[i] = this[i] } ~ op ~ q{ v;});
    }
}

/*** Keccak-f[1600] ***/
private void keccakf(ref State a) pure nothrow @safe @nogc
{
    ulong[5] b;
    ulong t = 0;
    ubyte x, y;
    static foreach (i; 0 .. 24)
    {
        // Theta
        x = 0;
        static foreach (_; 0 .. 5)
        {
            b[x] = 0;
            y = 0;
            static foreach (_; 0 .. 5)
            {
                b[x] ^= a[x + y];
                y += 5;
            }
            x++;
        }
        x = 0;
        static foreach (_; 0 .. 5)
        {
            y = 0;
            static foreach (_; 0 .. 5)
            {
                a[y + x] ^= b[(x + 4) % 5] ^ rol(b[(x + 1) % 5], 1);
                y += 5;
            }
            x++;
        }
        // Rho and pi
        t = a[1];
        x = 0;
        static foreach (_; 0 .. 24)
        {
            b[0] = a[pi[x]];
            a[pi[x]] = rol(t, rho[x]);
            t = b[0];
            x++;
        }
        // Chi
        y = 0;
        static foreach (_; 0 .. 5)
        {
            x = 0;
            static foreach (_; 0 .. 5)
            {
                b[x] = a[y + x];
                x++;
            }
            x = 0;
            static foreach (_; 0 .. 5)
            {
                a[y + x] = b[x] ^ ((~b[(x + 1) % 5]) & b[(x + 2) % 5]);
                x++;
            }
            y += 5;
        }
        // Iota
        a[0] ^= RC[i];
    }
}

private void xorin(ubyte[] dst, const ubyte[] src, ulong len) pure nothrow @safe @nogc
{
    foreach (i; 0 .. len)
    {
        dst[i] ^= src[i];
    }
}

deprecated
{
    /// used for create keccak family functions
    /// Params:
    ///   dst = destination for hash result
    ///   outlen = hash size wanted
    ///   src = value used for hash
    ///   inlen = size of value
    /// Returns: error code -1 if failed
    /// Deprecated: use hashImpl instead
    int hash(ubyte delim, ulong bits, bool checkSize = true)(ubyte* dst,
        ulong outlen, ubyte* src, ulong inlen) pure nothrow @trusted @nogc
    {
        return hashImpl!(delim, bits, checkSize)(dst[0 .. outlen], src[0 .. inlen]);
    }

    /// ditto
    alias keccak(ulong SIZE) = hash!(0x01, SIZE);

    ///
    @trusted unittest
    {
        ubyte[5] source = [0xff, 0xa1, 0x12, 0x22, 0xff];
        ubyte[256 / 8] result;
        ubyte[256 / 8] result2;
        assert(hash!(0x01, 256)(result.ptr, result.length, source.ptr, source.length) == 0);
        assert(keccak!(256)(result2.ptr, result2.length, source.ptr, source.length) == 0);
        assert(result == result2);
    }

}

/// used for create keccak family functions
/// Params:
///   dst = destination for hash result
///   src = value used for hash
/// Returns: error code -1 if failed
pure nothrow @safe @nogc int hashImpl(ubyte delim, ulong bits, bool checkSize = true)(
    ubyte[] dst, const(ubyte)[] src)
{
    static immutable rate = PLEN - (bits / 4);
    import core.stdc.string : memset, strncpy;

    static if (checkSize)
    {
        if (dst.length > (bits / 8))
        {
            return -1;
        }
    }
    static assert(rate < PLEN, "Rate should be less than 200");
    if ((dst == null) || ((src == null) && src.length != 0))
    {
        return -1;
    }

    State s;
    // Absorb input.
    while (src.length >= rate)
    {
        xorin(s.a[], src, rate);
        keccakf(s);
        src = src[rate .. $];
    }
    // Xor in the DS and pad frame.
    s.a[src.length] ^= delim;
    s.a[rate - 1] ^= 0x80;
    // Xor in the last block.
    xorin(s.a[], src, src.length);
    // Apply P
    keccakf(s);
    // Squeeze output.
    while (dst.length >= rate)
    {
        dst[0 .. rate] = s.a[0 .. rate];
        keccakf(s);
        dst = dst[rate .. $];
    }
    dst[0 .. dst.length] = s.a[0 .. dst.length];
    s.a[0 .. PLEN] = 0;
    return 0;
}

/// ditto
alias keccakImpl(ulong SIZE) = hashImpl!(0x01, SIZE);

///
@trusted unittest
{
    ubyte[5] source = [0xff, 0xa1, 0x12, 0x22, 0xff];
    ubyte[256 / 8] result;
    ubyte[256 / 8] result2;
    assert(hashImpl!(0x01, 256)(result[], source[]) == 0);
    assert(keccakImpl!(256)(result2[], source[]) == 0);
    assert(result[] == result2[]);
}

deprecated
{
    /// Instance if keccak function
    alias keccak_256 = keccak!256;
    /// ditto
    alias keccak_224 = keccak!224;
    /// ditto
    alias keccak_384 = keccak!384;
    /// ditto
    alias keccak_512 = keccak!512;

    ///
    @safe unittest
    {
        ubyte[5] source = [0xff, 0xa1, 0x12, 0x22, 0xff];
        ubyte[224 / 8] result;
        ubyte[256 / 8] result2;
        assert(keccak_224(result.ptr, result.length, source.ptr, source.length) == 0);
        assert(keccak_256(result2.ptr, result2.length, source.ptr, source.length) == 0);
    }
}

/// Used for create keccak functions
/// Params:
///   data = The value used for hash
/// Returns: static array with result of hashing
auto keccakD(ulong SIZE)(const ubyte[] data) pure nothrow @safe @nogc
{
    ubyte[SIZE / 8] hash;
    keccakImpl!SIZE(hash, data);
    return hash;
}

///
@safe unittest
{
    ubyte[5] source = [0xff, 0xa1, 0x12, 0x22, 0xff];
    ubyte[256 / 8] result;
    result = keccakD!256(source);
}

/// Instance of keccakD function
alias keccak256 = keccakD!256;
/// ditto
alias keccak256 = keccakImpl!256;
/// ditto
alias keccak224 = keccakD!224;
/// ditto
alias keccak224 = keccakImpl!224;
/// ditto
alias keccak384 = keccakD!384;
/// ditto
alias keccak384 = keccakImpl!384;
/// ditto
alias keccak512 = keccakD!512;
/// ditto
alias keccak512 = keccakImpl!512;

///
@safe unittest
{
    ubyte[5] source = [0xff, 0xa1, 0x12, 0x22, 0xff];
    ubyte[256 / 8] result1;
    ubyte[4] result2;
    result1 = keccak256(source);
    assert(keccak256(result2, source) == 0);
}

unittest
{
    import std;

    ubyte[] a = cast(ubyte[]) "hello".to!(char[]);
    ubyte[4] b;

    assert(keccak224(b, a) == 0);
    auto hash = b[].toHexString;
    assert("45524EC4" == hash);

    assert(keccak256(b[], a) == 0);
    hash = b[].toHexString;
    assert("1C8AFF95" == hash);

    assert(keccak384(b, a) == 0);
    hash = b[].toHexString;
    assert("DCEF6FB7" == hash);

    assert(keccak512(b, a) == 0);
    hash = b[].toHexString;
    assert("52FA8066" == hash);
}

unittest
{
    import std;

    ubyte[] a = cast(ubyte[]) "hello".to!(char[]);
    {
        auto b = keccak224(a);

        auto hash = b[0 .. 4].toHexString;
        assert("45524EC4" == hash);
    }
    {
        auto b = keccak256(a);

        auto hash = b[0 .. 4].toHexString;
        assert("1C8AFF95" == hash);
    }
    {
        auto b = keccak384(a);

        auto hash = b[0 .. 4].toHexString;
        assert("DCEF6FB7" == hash);
    }
    {
        auto b = keccak512(a);

        auto hash = b[0 .. 4].toHexString;
        assert("52FA8066" == hash);
    }
}

unittest
{
    import std.datetime.stopwatch : benchmark;
    import std : uniform;
    import std : writeln;

    string t = import(`testText.txt`);

    benchmark!({ ubyte[32] o; keccak256(o, cast(ubyte[]) t); })(1).writeln;
    benchmark!({
        ubyte[32] o;
        immutable s = uniform(0, cast(long) t.length - 600);
        auto slice = t[s .. s + 600];
        keccak256(o, cast(ubyte[]) slice);
    })(10_000).writeln;
}

static assert(keccak256(cast(ubyte[]) "hello")[0] == cast(ubyte) 28u);
